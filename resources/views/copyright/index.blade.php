@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/marjolein.jpg" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Copyright</div>

                <div class="panel-body">
                    Op alle foto’s en teksten toebehorend aan het domein van stichtingtaaierakkers.nl rust het auteursrecht.
                    Publicatie of ander gebruik van ons auteursrechtelijk beschermde teksten en/of beeldmateriaal
                    zonder onze uitdrukkelijke  toestemming is een inbreuk op het auteursrecht.
                    Dit wordt zonder uitzondering beantwoord met een schikkingsvoorstel via een gespecialiseerd bedrijf.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

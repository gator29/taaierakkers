<!DOCTYPE html>
<html lang="nl">
<head>
	<?php
	    $detect = new Mobile_Detect;
	?>
    <meta name="google-site-verification" content="_RrSR-5GBBGPD41pRPCj5oauwM-XtqNKtxvHxq_kinU" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="alternate" href="https://stichtingtaaierakkers.nl/" hreflang="nl-nl" />
	<link rel="alternate" href="https://www.stichtingtaaierakkers.nl/" hreflang="nl-nl" />
	<link rel="alternate" href="http://stichtingtaaierakkers.nl/" hreflang="nl-nl" />
	<link rel="alternate" href="http://www.stichtingtaaierakkers.nl/" hreflang="nl-nl" />


    <title>Stichting Taaie Rakkers</title>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ URL::to('/') }}/ckeditor/ckeditor.js"></script>
    @if (Request::is('contact'))
	<!-- for google maps -->
		@if ($detect->isiOS())
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSWWnNnYN6TlcoUJIz_cBxcENUJYzrbp0"></script>
		@elseif ($detect->isAndroidOS())
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSWWnNnYN6TlcoUJIz_cBxcENUJYzrbp0"></script>
		@else
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSWWnNnYN6TlcoUJIz_cBxcENUJYzrbp0"></script>
		@endif
        <script src="{{ URL::to('/') }}/js/map.js"></script>
    @endif
    <!-- for the font -->
    <script src="https://use.typekit.net/eiy7aft.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    @if (Auth::user())
        <?php
	    //make session when user is logged in
	    @session_start();
	    $_SESSION['KCFINDER'] = array(
		'disabled' => false
	    );
	?>
    @endif

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link media="all" type="text/css" rel="stylesheet" href="{{ URL::to('/') }}/css/style.css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>

    </style>

    <!-- Begin cookiescript -->

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#000"
        },
        "button": {
          "background": "#f1d600"
        }
      }
    })});
    </script>

    <!-- Eind cookiescript -->

</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ URL::to('/') }}/kcfinder/upload/images/logo.png" alt="logo" class="img-responsive"></a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="{{ Request::path() == '/' ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
                    <li class="dropdown{{ Request::path() == 'cf' ? ' active' : '' }} {{ Request::path() == 'links' ? ' active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Informatie <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('cf') }}">Cystic Fibrosis</a></li>
                            <li><a href="{{ url('links') }}">Links</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::path() == 'story' ? 'active' : '' }}"><a href="{{ url('story') }}" class="verhaal_menu_item">Verhalen</a></li>
                    <li class="{{ Request::path() == 'news' ? 'active' : '' }}"><a href="{{ url('news') }}" class="nieuws_menu_item">Nieuws</a></li>
                    <li class="{{ Request::path() == 'sponsor' ? 'active' : '' }}"><a href="{{ url('sponsor') }}">Sponsors</a></li>
                    <!-- dropdown contact -->
                    <li class="dropdown {{ Request::path() == 'contact' ? ' active' : '' }} {{ Request::path() == 'inschrijven' ? ' active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Contact <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('contact') }}">Contact</a></li>
                            <li><a href="{{ url('inschrijven') }}">Inschrijven</a></li>
                        </ul>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())

                    @else
                        <li class="dropdown {{ Request::path() == 'register' ? ' active' : '' }} {{ Request::path() == 'admin' ? ' active' : '' }} {{ Request::path() == 'news/add' ? ' active' : '' }}">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('register') }}">Registreer een admin</a></li>
                                <li><a href="{{ url('admin') }}">Maak Verhaal</a></li>
                                <li><a href="{{ url('news/add') }}">Maak Nieuws</a></li>
                                <li><a href="{{ url('logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <footer>
        <div class="footer">
            <div class="container"></div>
            <div class="col-md-4">
                    <strong class="footer_titel">Over Stichting Taaie Rakkers</strong>
                    <div class="footer_text">
                        Stichting Taaie Rakkers vervult wensen voor<br> CF-Patiënten. Deze vind u allemaal terug op<br> deze website.
                    </div>
            </div>
            <div class="col-md-4 menu1">
                    <strong>Volgen</strong>

                        <ul class="menu social">
                            <li class="facebook li">
                                <a href="https://www.facebook.com/taaierakkers?fref=ts" target="_blank" class="footerlink">Facebook</a>
                            </li>
                        </ul>

            </div>
            <div class="col-md-4 divers">
                <strong>Divers</strong>

                    <ul class="menu2">
                        <li class="copy li">
                            <a href="copyright" class="footerlink">Copyright</a>
                        </li>
                    </ul>

            </div>
        </div>
    </footer>

    <!-- JavaScripts -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>

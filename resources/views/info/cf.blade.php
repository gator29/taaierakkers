@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/slide4.png" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Cystic Fibrosis</div>

                <div class="panel-body">
                    <p>Cystic Fibrosis (CF, taaislijmziekte) is een ongeneeslijke, erfelijke aandoening. Ieder mens heeft
                    kliertjes in het lichaam die slijm afscheiden. Dit slijm voert afvalstoffen af, zoals ingeademde 
                    stofdeeltjes en bacteriën. Daarnaast transporteert het verteringsstoffen van de alvleesklier naar de 
                    dunne darm. Bij mensen met CF is het slijm door een genetische afwijking uitzonderlijk taai. Het kan 
                    daardoor zijn functies onvoldoende vervullen, met als gevolg dat organen zoals de longen, alvleesklier 
                    en lever steeds slechter gaan functioneren.</p>

                    <h2 class="sub">Taai slijm</h2>

                    <p>Het CFTR-gen zorgt voor de productie van het CFTR-eiwit (de 'cystic fibrosis transmembrane 
                    conductance regulator'). De CFTR-eiwitten vormen een soort kanaaltjes in de celwand 
                    (~transmembrane) die het transport (~conductance) van water en zout in en uit de cellen regelen 
                    (~regulator). Wanneer dit CFTR-eiwit niet goed wordt aangemaakt, wordt het zout-watertransport 
                    verstoord, waardoor taai slijm ontstaat.</p>

                    <h2 class="sub">De belangrijkste symptomen van CF zijn:</h2>

                        <div class="col-md-6">
                            <ul>
                                <li>Langdurig hoesten en slijm opgeven</li>
                                <li>Terugkerende luchtweginfecties</li>
                                <li>Sterk naar zout smakend zweet</li>
                                <li>Weinig eetlust (vooral bij luchtweginfecties)</li>
                                <li>Groeiachterstand</li>
                            </ul>
                        </div>
                        <div class="col-md-6 symptom">
                            <ul>
                                <li>Buikpijn</li>
                                <li>Obstipatie</li>
                                <li>Vettige ontlasting</li>
                                <li>Verminderde vruchtbaarheid</li>
                            </ul>
                        </div>
                    
                    <p class="italic under">Deze symptomen van CF doen zich niet bij alle patiënten gelijktijdig en in dezelfde mate voor.</p>
                    
                    <div class="col-md-6">
                    <h2 class="sub">De feiten over CF</h2>

                    <ul>
                        <li>Cystic Fibrosis is een ongeneeslijke, erfelijke aandoening</li>
                        <li>Kenmerk van CF is het taaie slijm dat door de slijmklieren in het hele lichaam wordt afgescheiden</li>
                        <li>CF leidt tot toenemende luchtweginfecties met onomkeerbare beschadiging van de longen en stoornissen in de spijsvertering</li>
                        <li>CF is de meest voorkomende erfelijke ziekte met een beperkte levensverwachting</li>
                    </ul>
                    </div>

                    <div class="col-md-6">
                    <h2 class="sub">Statistiek</h2>

                    <ul>
                    <li>Meer dan een half miljoen mensen in Nederland zijn (ongemerkt) drager van het CF-gen en kunnen dit overdragen aan hun kinderen</li>
                    <li>Bijna elke week wordt in Nederland een baby met CF geboren</li>
                    <li>De gemiddelde levensverwachting van iemand met CF is rond de 40 jaar</li>
                    <li>Er zijn op dit moment ongeveer 1.530 mensen met CF in Nederland, waarvan 650 kinderen.</li>
                    </ul>
                    </div>

                    <p>Bron: <a href="http://www.ncfs.nl/over-cystic-fibrosis" target="_blank" class="link">NCFS</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

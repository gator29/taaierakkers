@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/slide4.png" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Links</div>

                <div class="panel-body">
                    <center>
                    <p class="blauw"><strong>Cystic Fibrosis Organisaties:</strong></p>
                        <p>
                            <a href="http://www.ncfs.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/ncfs.jpg" class="img47"><br>
                            Nederlandse Cystic Fibrosis Stichting</a>
                        </p>
                        <p>
                            <a href="http://www.cfcafe.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/cf-cafe.jpg" class="img89"><br>
                            Het CF-Café</a>
                        </p>
                        <p>
                            <a href="https://www.ecfs.eu/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/ecfs.jpg" class="img57"><br>
                            European Cystic Fibrosis Society</a></a>
                        </p>
                        <p>
                            <a href="http://www.cfww.org/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/cfww.jpg" class="img36"><br>
                            Cystic Fibrosis World Wide</a>
                        </p>
                    <p class="blauw"><strong>Wensstichtingen en andere leuke uitjes voor chronisch zieken</strong></p>
                        <p>
                            <a href="http://www.makeawishnederland.org/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/wish.jpg" class="img36"><br>
                            Make a wish Nederland</a>
                        </p>
                        <p>
                            <a href="https://www.stichtinghoogvliegers.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/hoogvlieger.jpg" class="img84"><br>
                            Stichting Hoogvliegers</a>
                        </p>
                        <p>
                            <a href="http://www.clownsduoharten2.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/harten2.jpg" class="img84"><br>
                            Clownsduo Harten 2</a>
                        </p>
                        <p>
                            <a href="http://www.opkikker.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/opkikker.jpg" class="img30"><br>
                            Stichting Opkikker</a>
                        </p>
                        <p>
                            <a href="http://www.villapardoes.nl" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/pardoes.jpg" class="img52"><br>
                            Villa Pardoes</a>
                        </p>
                        <p>
                            <a href="http://www.sailingkids.eu/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/sailing.jpg" class="img52"><br>
                            Stichting Sailing Kids</a>
                        </p>
                        <p>
                            <a href="http://www.onefineday.nl" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/onefineday.jpg" class="img113"><br>
                            Stichting One Fine Day</a>
                        </p>
                        <p>
                            <a href="http://www.stichtingvaarwens.nl/" target="_blank" class="link"><img src="{{ URL::to('/') }}/img/vaarwens.jpg" class="img84"><br>
                            Stichting Vaarwens</a>
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

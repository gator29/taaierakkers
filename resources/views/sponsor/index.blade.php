@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

			<img src="/kcfinder/upload/images/slide3.png" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Sponsors</div>

                <div class="panel-body">

					<div class="row">
						<div class="sponsorimages flex">

							<div class="flex"><a href="http://hoefslag.nl/" target="_blank"><img src="/kcfinder/upload/images/hoefslag.png" alt="logo hoefslag"></a></div>

							<div class="flex"><a href="http://www.albelli.nl/" target="_blank"><img src="/kcfinder/upload/images/Albelli.png" alt="logo albelli"></a></div>

							<div class="flex"><a href="http://www.emerparkauto.nl/" target="_blank"><img src="/kcfinder/upload/images/Emerpark-logo.png" alt="logo emerpark"></a></div>

						</div>
					</div>

					<div class="row">
						<div class="sponsorimages flex">
						   
						  	<div class="flex"><a href="http://www.kroese.nl/" target="_blank"><img src="/kcfinder/upload/images/kroese.png" alt="logo kroese"></a></div>
						  
						  	<div class="flex"><a href="http://www.gfps.com/" target="_blank"><img src="/kcfinder/upload/images/GF.png" alt="logo gf"></a></div>
						  
						  	<div class="flex"><a href="http://www.mcdonalds.nl/" target="_blank"><img src="/kcfinder/upload/images/mcdonalds.png" alt="logo mcdonalds"></a></div>
						
						</div>
					</div>

					<div class="row">
						<div class="sponsorimages flex">
						  
							<div class="flex"><a href="http://www.nieuwenhuijse.nl/" target="_blank"><img src="/kcfinder/upload/images/volvo.png" alt="logo nieuwenhuijse"></a></div>
							  
							<div class="flex"><a href="https://www.wensink.nl" target="_blank"><img src="/kcfinder/upload/images/wensink.png" alt="logo wensink" class="politie"></a></div>
							  
							<div class="flex"><a href="https://www.autohaas.nl/" target="_blank"><img src="/kcfinder/upload/images/autohaas.png" alt="logo autohaas" class="landrover"></a></div>

						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

@endsection

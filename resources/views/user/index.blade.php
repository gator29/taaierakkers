@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
          @if(Session::has('flash_message'))
            <div class="alert alert-success">
              {{ Session::get('flash_message') }}
            </div>
          @endif
          @include('common.errors')
            <div class="panel panel-default">
                <div class="panel-heading">Nieuwe gebruiker</div>

                <div class="panel-body">
                   <div class="form form_swek">
                        <form class="form-horizontal form_register_swek" role="form" method="POST" action="{{ url('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}  form_register">
                                <label class="col-md-4 control-label">Naam</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control form_register" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} form_register">
                                <label class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} form_register">
                                <label class="col-md-4 control-label">Wachtwoord</label>

                                <div class="col-md-6">
                                    
                                    <input type="password" class="form-control" name="password" value="{{ old('password') }}">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} form_register">
                              <label class="col-md-4 control-label">Bevestig wachtwoord</label>

                                <div class="col-md-6">
                                  <input type="password" class="form-control" name="password_confirmation">

                                  @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                  @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i>submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

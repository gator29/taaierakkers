@extends('layouts.app')

@section('content')

<script>
$(document).ready(function(){
    $('.nieuws_menu_item').addClass('active_nieuws');
});
</script>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/slide2.jpg" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>{!! html_entity_decode($news->title) !!}</h1></div>

                <div class="panel-body">
                    <p class="lead">{!! html_entity_decode($news->report) !!}</p>
                    <hr>

                    @if (Auth::guest())
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('news.index') }}" class="btn btn-info">Terug naar alle nieuws items</a>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-6">
                        <a href="{{ route('news.add') }}" class="btn btn-info">Terug naar alle nieuws items</a>
                        <a href="{{ route('news.edit', $news->id) }}" class="btn btn-primary">Bewerk nieuws item</a>
                        </div>
                        <div class="col-md-6 text-right">
                            {!! Form::open([
                                'method' => 'DELETE',
                                'route' => ['news.delete', $news->id]
                            ]) !!}
                                {!! Form::submit('Verwijder dit nieuws item?', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
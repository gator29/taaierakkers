@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Bewerk nieuws item</div>

                <div class="panel-body">

                    <p class="lead">Bewerk het nieuws items hier beneden of <a href="{{ route('news.add') }}">ga terug naar alle nieuws items.</a></p>
                    <hr>

                    @if(Session::has('flash_message'))
                                        <div class="alert alert-success">
                                            {{ Session::get('flash_message') }}
                                        </div>
                                    @endif
                                    @include('common.errors')

                    {!! Form::model($news, [
                        'method' => 'PATCH',
                        'route' => ['news.update', $news->id]
                    ]) !!}

                    <div class="form-group">
                        {!! Form::label('title', 'Titel:', ['class' => 'control-label']) !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('report', 'Nieuws:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('report', null, ['class' => 'news', 'id' => 'news']) !!}
                    </div>

                    {!! Form::submit('Bewerk Nieuws', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace( 'news', {
            filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
            filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images'
        });
</script>

@endsection

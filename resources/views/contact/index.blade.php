@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

			<img src="/kcfinder/upload/images/21.jpg" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Contact</div>

                <div class="panel-body">

					<div id="attachment_41" class="foto">
						<img alt="" src="/kcfinder/upload/images/bestuur-taaierakkers-300x225.jpg" />
						<p class="bestuur">links: Chris Kievit, rechts: Bert Huisjes</p>
					</div>



					<p class="tel">
						<strong class="italic inline">Voor telefonisch contact:</strong><br>
						Bert Huisjes   06 – 21 449 131<br>
						Chris Kievit    06 – 38 228 656<br>
						Wanneer u ons wilt bellen, gelieve dit tussen 17.00  en 20.00  te doen.
					</p>

					<p class="schrift">
						<strong class="italic">Voor schriftelijk contact:</strong><br>
						Stichting Taaie Rakkers<br>
						De Bereklauw 1<br>
						7702 AC Dedemsvaart   
					</p>


					<div id="map-canvas"></div><br>

					<p class="email">
						<strong class="italic">Email:</strong> <a href="mailto:info@stichtingtaaierakkers.nl"  onclick="window.open(this.href, 'test', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=no,dependent=no'); return false;" class="link">info@stichtingtaaierakkers.nl</a>
					</p>

					<p class="overig" >
						<strong class="italic">Overige gegevens:</strong><br>
						Kamer van Koophandel: 34168299<br>
						Iban rekening nr: NL18 BUNQ 2290 1718 75
					</p>

					<p class="msg">
						<h1>Contact Stichting Taaie Rakkers</h1>
						<p>Vul alstublieft uw gegevens in en daaronder een kort bericht. We zullen dan zo spoedig mogelijk contact met u opnemen.<br>
						<!-- mail form -->
						<ul>
						    @foreach($errors->all() as $error)
						        <li>{{ $error }}</li>
						    @endforeach
						</ul></p>

						{!! Form::open(array('route' => 'mail', 'class' => 'form')) !!}

						<div class="form-group">
						    {!! Form::label('Naam:') !!}
						    {!! Form::text('name', null, 
						        array('required', 
						              'class'=>'form-control', 
						              'placeholder'=>'Uw Naam')) !!}
						</div>

						<div class="form-group">
						    {!! Form::label('Emailadres:') !!}
						    {!! Form::text('email', null, 
						        array('required', 
						              'class'=>'form-control', 
						              'placeholder'=>'Uw Emailadres')) !!}
						</div>

						<div class="form-group">
						    {!! Form::label('Bericht:') !!}
						    {!! Form::textarea('message', null, 
						        array('required', 
						              'class'=>'form-control',
							      'id'=>'message',
						              'placeholder'=>'Uw Bericht')) !!}
						</div>
						<div class="form-group">
						    {!! Recaptcha::render() !!}
						</div>
						<div class="form-group">
						    {!! Form::submit('Bericht Versturen', 
						      array('class'=>'btn btn-primary')) !!}
						</div>
						{!! Form::close() !!}

					</p>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

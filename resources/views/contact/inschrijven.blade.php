@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

			<img src="/kcfinder/upload/images/21.jpg" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Inschrijven</div>

                <div class="panel-body">
					<p>
						Iedereen kan een CF-patiënt inschrijven voor een persoonlijke wens/verwendag.
						Dit kan zijn een ouder, vriend, vriendin, partner enz.
					</p>

					<p>
						Via onderstaande link opent u een Word® document. Daarin staat hoe u dit dient in te vullen en op te sturen.
						Vult u de vragen a.u.b.  zo volledig mogelijk in.
					</p>

					<p><a href="/kcfinder/upload/files/WensformulierStichtingTaaieRakkers.doc" class="link">Wensformulier Stichting Taaie Rakkers</a></p>

					<p>
						Aan hij of zij die de wensdag ontvangt, is geen leeftijdsgrens gebonden.
						De enige  restrictie die wij hanteren  is dat de uitvoering van de wens in Nederland dient te worden gedaan.
						Het aantal personen dat mee kan op een dergelijke dag is afhankelijk van een aantal factoren.
						Wij zullen daarover contact met de “aanbrenger” hebben.
						Tijdens de dag zelf gaat het bestuur zelf mee.
						Zij zorgen voor de begeleiding, hebben contact met de sponsoren en maken foto’s
					</p>
					<p>
						Wanneer u iemand inschrijft voor een wens,
						dient u er zeker van te zijn dat hij/zij er geen probleem mee heeft dat de foto’s van de dag 
						openbaar worden gemaakt op de website en deze ook worden gedeeld met de sponsoren. 
						Mocht dit een probleem zijn, dit graag ruim van te voren aangeven.
					</p>

					<p>Het bestuur.</p>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
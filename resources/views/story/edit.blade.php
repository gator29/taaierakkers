@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Bewerk verhaal</div>

                <div class="panel-body">
                    <p class="lead">Bewerk het verhaal hier beneden of <a href="{{ route('admin') }}">ga terug naar alle verhalen.</a></p>
                    <hr>

                    @if(Session::has('flash_message'))
                                        <div class="alert alert-success">
                                            {{ Session::get('flash_message') }}
                                        </div>
                                    @endif
                                    @include('common.errors')

                    {!! Form::model($story, [
                        'method' => 'PATCH',
                        'route' => ['story.update', $story->id]
                    ]) !!}

                    <div class="form-group">
                        {!! Form::label('title', 'Titel:', ['class' => 'control-label']) !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('story', 'Verhaal:', ['class' => 'control-label']) !!}
                        {!! Form::textarea('story', null, ['class' => 'story', 'id' => 'story']) !!}
                    </div>

                    {!! Form::submit('Bewerk Verhaal', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
    

<script>
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
CKEDITOR.replace( 'story', {
            filebrowserImageBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images',
            filebrowserBrowseUrl: '/kcfinder/browse.php?opener=ckeditor&type=images'
        });
</script>
@endsection

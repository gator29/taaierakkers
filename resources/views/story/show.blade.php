@extends('layouts.app')

@section('content')

<script>
$(document).ready(function(){
    $('.verhaal_menu_item').addClass('active_verhaal');
});
</script>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/slide5.png" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading"><h1>{!! html_entity_decode($story->title) !!}</h1></div>

                <div class="panel-body">
                    <p class="lead">{!! html_entity_decode($story->story) !!}</p>
                    <hr>

                    @if (Auth::guest())
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('story.index') }}" class="btn btn-info">Terug naar alle verhalen</a>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-6">
                     	<a href="{{ route('admin') }}" class="btn btn-info">Terug naar alle verhalen</a>
                    	<a href="{{ route('story.edit', $story->id) }}" class="btn btn-primary">Edit Story</a>
                    	</div>
                        <div class="col-md-6 text-right">
                            {!! Form::open([
                                'method' => 'DELETE',
                                'route' => ['story.delete', $story->id]
                            ]) !!}
                                {!! Form::submit('Verwijder dit verhaal?', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
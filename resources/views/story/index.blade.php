@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <img src="/kcfinder/upload/images/slide5.png" class="top_img" alt="">

            <div class="panel panel-default">
                <div class="panel-heading">Verhalen</div>
                <div class="panel-body">
					@forelse ($data as $r)
                        <a href="{{ route('story.show', $r->id) }}"><h3>{!! html_entity_decode($r->title) !!}</h3></a>
                        <!-- Maakt een linkje van de titel van het gerelateerde id -->
                        <hr>
                    @empty
                        <!-- Als er niks geplaatst is, ziet de gebruiker de tekst hieronder -->
                        Op dit moment zijn er nog geen verhalen, kijkt u later nog eens
                    @endforelse
                    <!-- Plaatst alle opgehaalde verhalen -->
                    {!! $data->links() !!}
                    <!-- Linkjes naar de volgende pagina -->
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
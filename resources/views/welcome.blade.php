@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
            <img src="/kcfinder/upload/images/marjolein.jpg" class="top_img" alt="">
            
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                   <p>Stichting Taaie Rakkers is de nieuwe naam van Stichting 112-ers helpen Cystic Fibrosis Patiënten.</p>

                    <strong class="doel">Doel van de stichting:</strong><br>

                    <p>Stichting Taaie Rakkers heeft het  doel om 4 tot 6 hartenwensen per jaar van CF-ers te vervullen.
                    Er is <strong>geen leeftijdsgrens</strong> aan de CF-er die de wensdag aangeboden krijgt. Tijdens die dag worden ook,
                    voor zover mogelijk, de andere gezinsleden betrokken. Dit kunnen zijn de ouders en broertjes/zusjes of partner
                    en/of eventueel eigen kinderen. Het belang van deze dagen is het even “vergeten” van ziek zijn. Gewoon genieten
                    van dat wat je altijd al had willen doen, zien, horen…… Uit het isolement komen. Een wensdag kan uitgebreid zijn
                    of juist heel simpel. Het belangrijkste van een dergelijke dag is: een dag niet ziek zijn en genieten van alle
                    verrassingen.</p>

                    <p>Op deze website kunt u een aantal verslagen lezen van de door ons georganiseerde wensdagen.
                    Naast deze verslagen, staat er bijna overal een persoonlijk verhaal bij geschreven. Daar dit <span class="italic">hun</span> verhaal is,
                    zullen daar geen correcties op worden toegepast.</p>

                    <p>De wensen worden mogelijk gemaakt door sponsorbijdragen.
                    Deze staan bij de betreffende wens vermeld.  Zij zullen een bezoek aan hun website zeer op prijs stellen,
                    zonder hun bijzondere bijdrage zou de wensdag niet mogelijk zijn geweest.</p>

                    <p>Sponsoren die ons met regelmaat helpen, staan daarnaast vermeld op deze pagina.</p>

                    <p><strong>Verdere gegevens: <a class="link" href="contact">zie contact</a></strong></p>

                    <p class="copyright">
                        Voor u materiaal van deze site gebruikt voor publicatie of andere doeleinden, lees eerst de tekst op 
                        <a class="link" href="copyright">deze pagina.</a>
                    </p>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\ContactFormRequest;

use mail;

class ContactController extends Controller
{
    public function index() {

        return view('contact.index');

    }

    public function mail(ContactFormRequest $request) {

        $this->validate($request, [
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

    	\Mail::send('emails.contact',
        $data = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message')
        ), function($message) use ($data)
	    {
		$emails = ['info@stichtingtaaierakkers.nl'];

	        $message->from($data['email']);
	        $message->to($emails, 'Admin')->subject('Stel hier uw vraag of opmerking');
	    });

    	return \Redirect::route('contact.index')
      		->with('message', 'Uw bericht is verzonden!');

    }

    public function inschrijven() {

        return view('contact.inschrijven');

    }
}

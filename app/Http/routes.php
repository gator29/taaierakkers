<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    // laravel auth pages
    Route::auth();

    // go to pages

    // admin pages
    Route::get('admin', ['as' => 'admin', 'uses' => 'AdminController@index']);

    Route::get('story/edit/{id}', ['as' => 'story.edit', 'uses' => 'AdminController@editStory']);

    Route::get('news/add', ['as' => 'news.add', 'uses' => 'AdminController@news']);

    Route::get('news/edit/{id}', ['as' => 'news.edit', 'uses' => 'AdminController@editNews']);

    Route::get('register', ['as' => 'user.index', 'uses' => 'AdminController@register']);

    Route::get('upload', 'AdminController@upload');

    // home page
	Route::get('/', ['as' => '/', 'uses' => 'HomeController@index']);

	// story pages
	Route::get('story', ['as' => 'story.index', 'uses' => 'StoryController@index']);

	Route::get('story/show/{id}', ['as' => 'story.show', 'uses' => 'StoryController@show']);

	// news pages
	Route::get('news', ['as' => 'news.index', 'uses' => 'NewsController@index']);

	Route::get('news/show/{id}', ['as' => 'news.show', 'uses' => 'NewsController@show']);

	// contact pages
	Route::get('contact', ['as' => 'contact.index', 'uses' => 'ContactController@index']);

	Route::get('inschrijven', ['as' => 'contact.inschrijven', 'uses' => 'ContactController@inschrijven']);

	// info pages
	Route::get('cf', ['as' => 'info.cf', 'uses' => 'InfoController@index']);

	Route::get('links', ['as' => 'info.links', 'uses' => 'InfoController@links']);

	// copyright page
	Route::get('copyright', ['as' => 'copyright.index', 'uses' => 'CopyrightController@index']);

	Route::get('sponsor', ['as' => 'sponsor.index', 'uses' => 'SponsorController@index']);

	//update stuff
	Route::patch('story/{id}/update', ['as' => 'story.update', 'uses' => 'AdminController@updateStory']);

	Route::get('story/{id}/restore', ['as' => 'story.restore', 'uses' => 'AdminController@restoreStory']);

	Route::patch('news/{id}/update', ['as' => 'news.update', 'uses' => 'AdminController@updateNews']);

	Route::get('news/{id}/restore', ['as' => 'news.restore', 'uses' => 'AdminController@restoreNews']);

	//post stuff
	Route::post('story', 'AdminController@storeStory');

	Route::post('news', 'AdminController@storeNews');

	Route::post('contact', ['as' => 'mail', 'uses' => 'ContactController@mail']);

	Route::post('register', 'AdminController@storeUser');

	//soft delete stuff
	Route::delete('story/{id}/delete', ['as' => 'story.delete', 'uses' => 'AdminController@deleteStory']);

	Route::delete('news/{id}/delete', ['as' => 'news.delete', 'uses' => 'AdminController@deleteNews']);

});
